# Version 1.1.1

Fixed a bug with alignment of fields packed/unpacked from unaligned
offsets. The resulting structure was not consistent with `format-size`
and other language's implementations of `pack`/`unpack`.
More details in [issue #1](https://github.com/weinholt/struct-pack/issues/1).
A side-effect of this change is better code from the `pack` syntax.

The package no longer includes `(struct pack private)`. This will
eliminate import phase issues expired with some Schemes.

The order of the setters in the expansion of `pack` is now in reverse
order. This means that the highest index is written first and
therefore range checked first, which allows a sufficiently smart
compiler to eliminate all other range and type checks on the
bytevector.

# Version 1.1.0

New procedure: `put-pack`.

# Version 1.0.0

Initial release split out from Industria.
